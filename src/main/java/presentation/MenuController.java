package presentation;

import business.Restaurant;
import business.RestaurantProcessing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuController {
    private MenuGUI menuGUI;
    private RestaurantProcessing restaurant;

    public MenuController(RestaurantProcessing restaurant) {
        this.restaurant = restaurant;
        menuGUI = new MenuGUI();

        menuGUI.addWaiterListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menuGUI.dispose();
                new WaiterController(restaurant);
            }
        });
        menuGUI.addAdminListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menuGUI.dispose();
                new AdministratorController(MenuController.this.restaurant);
            }
        });
    }

    public static void main(String[] args) {
        RestaurantProcessing restaurantProcessing = new Restaurant();

        new MenuController(restaurantProcessing);
    }
}
