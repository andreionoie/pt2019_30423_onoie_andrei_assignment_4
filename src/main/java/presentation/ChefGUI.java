package presentation;

import business.Order;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class ChefGUI implements Observer {

    public ChefGUI() {

    }

    public void update(Observable o, Object arg) {
        JOptionPane.showMessageDialog(null, "New order: " + (Order) arg);
    }


}
