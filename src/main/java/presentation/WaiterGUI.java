package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;
import business.MenuItem;

public class WaiterGUI extends JFrame {
    private int WIDTH=900, HEIGHT=600;
    private JSpinner tableNumberSpinner;
    private JButton addButton, billButton, backButton, orderButton, priceButton;
    private JTable menuItemsTable, currentOrderTable, ordersTable;
    private JPanel tablesPanel;

    public WaiterGUI () {
        addComponents();

        setTitle("Restaurant Management - Waiter Menu");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void addComponents() {
        JPanel controlPanel = new JPanel(new GridLayout(1, 11));
        tablesPanel = new JPanel(new GridLayout(1, 3));
        tableNumberSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 50, 1));

        addButton = new JButton("Add");
        priceButton = new JButton("Price");
        orderButton = new JButton("Order");
        billButton = new JButton("Bill");
        backButton = new JButton("Back");

        controlPanel.add(new JLabel("Select table:"));
        controlPanel.add(tableNumberSpinner);
        controlPanel.add(new JLabel());
        controlPanel.add(addButton);
        controlPanel.add(new JLabel());
        controlPanel.add(new JLabel());
        controlPanel.add(priceButton);
        controlPanel.add(orderButton);
        controlPanel.add(billButton);
        controlPanel.add(new JLabel());
        controlPanel.add(backButton);


        add(tablesPanel, BorderLayout.CENTER);
        add(controlPanel, BorderLayout.SOUTH);
    }

    public void drawTables(List<MenuItem> menu, List<MenuItem> currentOrder, List<Object> orders) {
        tablesPanel.removeAll();

        currentOrderTable = TableHelper.getPopulatedTable(currentOrder);
        currentOrderTable.setFocusable(false);
        currentOrderTable.setRowSelectionAllowed(false);
        currentOrderTable.setDefaultEditor(Object.class, null);
        currentOrderTable.setVisible(true);

        JScrollPane sp1 = new JScrollPane(currentOrderTable);
        sp1.setVisible(true);
        sp1.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        menuItemsTable = TableHelper.getPopulatedTable(menu);
        menuItemsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        menuItemsTable.setDefaultEditor(Object.class, null);
        menuItemsTable.setVisible(true);

        JScrollPane sp2 = new JScrollPane(menuItemsTable);
        sp2.setVisible(true);
        sp2.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

       ordersTable = TableHelper.getGenericPopulatedTable(orders);
       ordersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
       ordersTable.setDefaultEditor(Object.class, null);
       ordersTable.setVisible(true);

        JScrollPane sp3 = new JScrollPane(ordersTable);
        sp3.setVisible(true);
        sp3.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        tablesPanel.add(sp2);
        tablesPanel.add(sp1);
        tablesPanel.add(sp3);

        tablesPanel.revalidate();
        tablesPanel.repaint();
        tablesPanel.updateUI();
    }

    public int getSpinnerValue() {
        return (int) tableNumberSpinner.getValue();
    }

    public int getSelectedRowID() {
        if (menuItemsTable.getSelectedRow() < 0)
            return -1;

        return (int) menuItemsTable.getValueAt(menuItemsTable.getSelectedRow(), 0);
    }

    public int getSelectedOrderID() {
        if (ordersTable.getSelectedRow() < 0)
            return -1;

        return (int) menuItemsTable.getValueAt(ordersTable.getSelectedRow(), 0);
    }

    public void setAddListener(ActionListener al) {
        addButton.addActionListener(al);
    }

    public void setBillListener(ActionListener al) {
        billButton.addActionListener(al);
    }

    public void setBackListener(ActionListener al) {
        backButton.addActionListener(al);
    }

    public void setOrderListener(ActionListener al) {
        orderButton.addActionListener(al);
    }

    public void setPriceListener(ActionListener al) {
        priceButton.addActionListener(al);
    }
}
