package presentation;

import business.BaseProduct;
import business.MenuItem;
import business.RestaurantProcessing;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class AdministratorController {
    private AdministratorGUI administratorGUI;
    private RestaurantProcessing restaurant;

    public AdministratorController(RestaurantProcessing restaurant) {
        this.restaurant = restaurant;
        administratorGUI = new AdministratorGUI();

        administratorGUI.drawTable((List<MenuItem>) restaurant.getMenuItems());

        administratorGUI.setBackButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                administratorGUI.dispose();
                new MenuController(restaurant);
            }
        });

        administratorGUI.setAddButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    double cost = Double.parseDouble(administratorGUI.getCostField());
                    BaseProduct bp = new BaseProduct(restaurant.getMenuItems().size() + 1, cost, administratorGUI.getNameField());
                    restaurant.createMenuItem(bp);
                    administratorGUI.drawTable((List<MenuItem>) restaurant.getMenuItems());
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(administratorGUI, "Invalid values.");
                }
            }
        });

        administratorGUI.setDeleteButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                restaurant.deleteMenuItem((int) administratorGUI.getTableValueAt(administratorGUI.getSelectedRow(), 0));
                administratorGUI.drawTable((List<MenuItem>) restaurant.getMenuItems());
            }
        });

        administratorGUI.setTablesListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                administratorGUI.setLabels(administratorGUI.getTableValueAt(administratorGUI.getSelectedRow(), 0).toString(),
                        administratorGUI.getTableValueAt(administratorGUI.getSelectedRow(), 1).toString(),
                        administratorGUI.getTableValueAt(administratorGUI.getSelectedRow(), 2).toString());
            }
        });
    }

}
