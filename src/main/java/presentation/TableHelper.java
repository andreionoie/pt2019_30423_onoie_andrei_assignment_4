package presentation;

import business.BaseProduct;
import business.CompositeProduct;
import business.MenuItem;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class TableHelper {
    public static JTable getPopulatedTable(final List<MenuItem> objects) {
        if (objects == null || objects.isEmpty()) {
            return new JTable();
        }

        Class objClass = null;
        try {
            objClass = Class.forName("business.BaseProduct");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }

        final List<String> headers = new ArrayList<String>();
        for (Field f : objClass.getDeclaredFields()) {
            headers.add(f.getName());
        }

        TableModel tm = new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return objects.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                MenuItem i = objects.get(rowIndex);

                if (columnIndex == 0) {
                    if (i instanceof BaseProduct)
                        return ((BaseProduct) i).getId();
                    else if (i instanceof CompositeProduct)
                        return ((CompositeProduct) i).getId();
                    else
                        return "-";
                } else if (columnIndex == 1) {
                    return i.getName();
                } else {
                    return i.computePrice();
                }
            }

            @Override
            public String getColumnName(int i) {
                return headers.get(i);
            }
        };
        JTable t = new JTable();

        t.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        t.setModel(tm);
        resizeColumnWidth(t);

        return t;
    }

    public static JTable getGenericPopulatedTable(List<Object> objects) {
        if (objects == null || objects.isEmpty()) {
            return new JTable();
        }

        Class objClass = objects.get(0).getClass();

        List<String> headers = new ArrayList<String>();
        for (Field f : objClass.getDeclaredFields()) {
            headers.add(f.getName());
        }

        TableModel tm = new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return objects.size();
            }

            @Override
            public int getColumnCount() {
                return objClass.getDeclaredFields().length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                try {
                    Field f = objClass.getDeclaredFields()[columnIndex];
                    f.setAccessible(true);
                    Object out = f.get(objects.get(rowIndex));
                    f.setAccessible(false);
                    return out;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public String getColumnName(int i) {
                return headers.get(i);
            }
        };
        JTable t = new JTable();

        t.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        t.setModel(tm);
        resizeColumnWidth(t);

        return t;
    }

    private static void resizeColumnWidth(JTable table) {
        final TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            int width = 15; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width +1 , width);
            }
            if(width > 300)
                width=300;
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }
}
