package presentation;

import business.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class WaiterController {
    private WaiterGUI waiterGUI;
    private RestaurantProcessing restaurant;
    private List<MenuItem> orderItems;

    public WaiterController(RestaurantProcessing restaurantProcessing) {
        restaurant = restaurantProcessing;
        waiterGUI = new WaiterGUI();
        orderItems = new ArrayList<MenuItem>();

        waiterGUI.drawTables((List<MenuItem>) restaurant.getMenuItems(), orderItems, restaurant.getOrders());

        waiterGUI.setBackListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                waiterGUI.dispose();
                new MenuController(restaurant);
            }
        });

        waiterGUI.setAddListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MenuItem toBeAdded = null;
                int rowID = waiterGUI.getSelectedRowID();
                if (rowID < 0) {
                    JOptionPane.showMessageDialog(waiterGUI, "Select a menu item first.");
                    return;
                }

                for (MenuItem i : restaurant.getMenuItems()) {
                    if (i instanceof BaseProduct) {
                        if (((BaseProduct) i).getId() == rowID) {
                            toBeAdded = i;
                            break;
                        }
                    } else if (i instanceof CompositeProduct) {
                        if (((CompositeProduct) i).getId() == rowID) {
                            toBeAdded = i;
                            break;
                        }
                    }
                }

                orderItems.add(toBeAdded);
                waiterGUI.drawTables((List<MenuItem>) restaurant.getMenuItems(), orderItems, restaurant.getOrders());
            }
        });

        waiterGUI.setOrderListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (orderItems.isEmpty())
                    return;

                restaurant.createOrder(waiterGUI.getSpinnerValue(), new ArrayList<MenuItem>(orderItems));

                orderItems.clear();

                waiterGUI.drawTables((List<MenuItem>) restaurant.getMenuItems(), orderItems, restaurant.getOrders());
            }
        });

        waiterGUI.setPriceListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = waiterGUI.getSelectedOrderID();
                if (id < 0) {
                    JOptionPane.showMessageDialog(waiterGUI, "Select a valid order from the table.");
                    return;
                }

                Order foundOrder = null;

                for (Object o : restaurant.getOrders()) {
                    if (((Order) o).getOrderId() == id) {
                        foundOrder = (Order) o;
                        break;
                    }
                }

                if (foundOrder == null) {
                    return;
                }

                JOptionPane.showMessageDialog(waiterGUI, "Total price for this order: $" + restaurant.computeOrderPrice(foundOrder) + ".");
            }
        });

        waiterGUI.setBillListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = waiterGUI.getSelectedOrderID();
                if (id < 0) {
                    JOptionPane.showMessageDialog(waiterGUI, "Select a valid order from the table.");
                    return;
                }

                Order foundOrder = null;

                for (Object o : restaurant.getOrders()) {
                    if (((Order) o).getOrderId() == id) {
                        foundOrder = (Order) o;
                        break;
                    }
                }

                if (foundOrder == null) {
                    return;
                }

                restaurant.generateBill(foundOrder);
            }
        });
    }
}
