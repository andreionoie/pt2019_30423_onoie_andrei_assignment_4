package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class MenuGUI extends JFrame {
    private int WIDTH=400, HEIGHT=150;
    private JButton waiter, admin, chef;

    public MenuGUI() {
        addComponents();

        setTitle("Restaurant Management - Menu");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void addComponents() {
        JPanel panel = new JPanel(new GridLayout(1, 3));
        waiter = new JButton("Waiter");
        admin = new JButton("Administrator");
        chef = new JButton("Chef");

        panel.add(waiter);
        panel.add(admin);
        panel.add(chef);

        add(new JLabel("Select the part you want to manage:", JLabel.CENTER), BorderLayout.NORTH);
        add(panel, BorderLayout.CENTER);
    }

    public void addWaiterListener(ActionListener al) {
        waiter.addActionListener(al);
    }

    public void addAdminListener(ActionListener al) {
        admin.addActionListener(al);
    }

    public void addChefListener(ActionListener al) {
        chef.addActionListener(al);
    }

}