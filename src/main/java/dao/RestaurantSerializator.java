package dao;

import business.BaseProduct;
import business.CompositeProduct;
import business.MenuItem;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;

public class RestaurantSerializator {
    public static void writeMenuItems(ArrayList<MenuItem> items) {
        try {
            FileOutputStream fos = new FileOutputStream("menudata.bin");
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(items);
            oos.close();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static Collection<MenuItem> readMenuItems() {
        Collection<MenuItem> items = new ArrayList<MenuItem>();

        try {
            FileInputStream fis = new FileInputStream("menudata.bin");
            ObjectInputStream ois = new ObjectInputStream(fis);

            items = (ArrayList) ois.readObject();

            ois.close();
            fis.close();

            return items;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void main(String[] args) {
        Collection<MenuItem> items = new ArrayList<MenuItem>();

        BaseProduct bp1 = new BaseProduct(1, 12.5, "Cheeseburger");
        BaseProduct bp2 = new BaseProduct(2, 7.5, "French Fries");
        BaseProduct bp3 = new BaseProduct(3, 5, "Coca-Cola");
        BaseProduct bp4 = new BaseProduct(6, 1.5, "Ketchup");

        CompositeProduct cp1 = new CompositeProduct(4);
        cp1.addItem(bp1);
        cp1.addItem(bp2);
        cp1.addItem(bp3);

        items.add(bp1);
        items.add(bp2);
        items.add(bp3);
        items.add(cp1);
        items.add(new BaseProduct(5, 2, "Water"));


        writeMenuItems((ArrayList) items);

        Collection<MenuItem> itemsRead = readMenuItems();

        for (MenuItem i : itemsRead) {
            System.out.println(i);
        }

        System.out.println("modifiying..");

        itemsRead.add(bp4);
        CompositeProduct cp2 = new CompositeProduct(7);
        cp2.addItem(cp1);
        cp2.addItem(bp4);
        itemsRead.add(cp2);

        writeMenuItems((ArrayList) itemsRead);

        for (MenuItem i : itemsRead) {
            System.out.println(i);
        }
    }
}
