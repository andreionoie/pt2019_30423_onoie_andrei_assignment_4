package dao;

import business.MenuItem;
import business.Order;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

public class OrderBillWriter {
    public static void writeOrderToFile(Order o, Collection<MenuItem> items) {
        String filename = "order_" + o.orderId + ".txt";
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
            bw.write("OrderBill #" + o.orderId + "\n");
            bw.write("Table " + o.table + ", date " + o.date + "\n");
            bw.write("Content:\n");

            double sum=0;

            for (MenuItem i : items) {
                sum += i.computePrice();
                bw.write(i.toString() + "\n");
            }

            bw.write("-------------------------\n");
            bw.write("Total cost: $");

            bw.write(sum + "\n");
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
