package business;

public abstract class MenuItem {
    public abstract double computePrice();
    public abstract String getName();
}
