package business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem implements Serializable {
    private int id;
    private List<MenuItem> products;

    public CompositeProduct(int id) {
        this.id = id;
        products = new ArrayList<MenuItem>();
    }

    public void addItem(MenuItem i) {
        products.add(i);
    }

    public double computePrice() {
        double sum=0;

        for (MenuItem i : products)
            sum += i.computePrice();

        return sum;
    }

    @Override
    public String getName() {
        List<String> names = new ArrayList<>();

        for (MenuItem i : products)
            names.add(i.getName());

        return String.join(", ", names);
    }

    @Override
    public String toString() {
        return "$" + computePrice() + "     " + getName();
    }

    public int getId() {
        return id;
    }
}
