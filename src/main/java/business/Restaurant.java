package business;

import dao.OrderBillWriter;
import dao.RestaurantSerializator;
import presentation.ChefGUI;

import java.text.SimpleDateFormat;
import java.util.*;

// class invariant: menuItems and orderMap are never null
// observer class is always notfied on placing order
public class Restaurant extends Observable implements RestaurantProcessing {
    private Collection<MenuItem> menuItems;
    private Map<Order, Collection<MenuItem>> orderMap;

    public Restaurant() {
        menuItems = RestaurantSerializator.readMenuItems();
        orderMap = new HashMap<>();
        addObserver(new ChefGUI());
    }

    // admin op
    @Override
    public void createMenuItem(MenuItem i) {
        assert i.computePrice() > 0;
        assert i.getName().equals("") != true;

        int size = menuItems.size();
        menuItems.add(i);
        RestaurantSerializator.writeMenuItems((ArrayList<MenuItem>) menuItems);

        assert size == menuItems.size() - 1;
    }

    @Override
    public void deleteMenuItem(int id) {
        assert id <= menuItems.size();
        MenuItem toBeRemoved = null;

        for (MenuItem i : menuItems) {
            if (i instanceof BaseProduct) {
                if (((BaseProduct) i).getId() == id) {
                    toBeRemoved = i;
                    break;
                }
            } else if (i instanceof CompositeProduct) {
                if (((CompositeProduct) i).getId() == id) {
                    toBeRemoved = i;
                    break;
                }
            }
        }

        int size = menuItems.size();
        menuItems.remove(toBeRemoved);
        RestaurantSerializator.writeMenuItems((ArrayList<MenuItem>) menuItems);

        assert size == menuItems.size() + 1;
    }

    public void editMenuItem() {

    }

    // waiter op
    public void createOrder(int tableNo, Collection<MenuItem> items) {
        assert tableNo > 0;
        String date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
        Order o = new Order(getOrders().size() + 1, date, tableNo);

        int size = orderMap.size();
        orderMap.put(o, items);
        setChanged();
        notifyObservers(o);
        assert size == orderMap.size() - 1;
    }

    public double computeOrderPrice(Order o) {
        assert orderMap.keySet().contains(o);
        double sum = 0;

        for (MenuItem i : orderMap.get(o)) {
            sum += i.computePrice();
        }

        assert sum > 0;
        return sum;
    }

    public void generateBill(Order o) {
        OrderBillWriter.writeOrderToFile(o, orderMap.get(o));
    }

    public Collection<MenuItem> getMenuItems() {
        return menuItems;
    }

    @Override
    public List<Object> getOrders() {
        return new ArrayList<Object>(orderMap.keySet());
    }
}
