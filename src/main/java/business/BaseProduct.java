package business;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable {
    private int id;
    private String name;
    private double price;

    public BaseProduct(int id, double price, String name) {
        this.id = id;
        this.price = price;
        this.name = name;
    }

    public double computePrice() {
        return this.price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "$" + price + "      " + name;
    }

}
