package business;

import java.util.Collection;
import java.util.List;

public interface RestaurantProcessing {
    // pre: item has valid attributes
    // post: item is added to the menu
    void createMenuItem(MenuItem i);
    // pre: item id is found in menu
    // post: item is deleted
    void deleteMenuItem(int id);
    void editMenuItem();

    // pre: valid input
    // post: order is placed
    void createOrder(int tableNo, Collection<MenuItem> items);
    // pre: order exists
    // post: price is positive
    double computeOrderPrice(Order o);

    void generateBill(Order o);

    Collection<MenuItem> getMenuItems();
    List<Object> getOrders();
}
